# Loading libraries
#------Package List------

#------Chart------
install.packages("ggplot2")
library(ggplot2)

#------Linear Model------
install.packages('caTools')
library(caTools)

install.packages('dplyr')
library(dplyr)

#------Missing values------
install.packages('Amelia')
library(Amelia)

#------Correlations------ 
install.packages("corrplot")
library(corrplot)

#------Kmeans algorithm------
install.packages('cluster')
library(cluster)

#------SQL------
install.packages('RSQLite')
library(RSQLite)

install.packages('sqldf')
library(sqldf)

#------Text mining / Naive base------
install.packages('tm')
library(tm)

install.packages('e1071')
library(e1071)

install.packages('wordcloud')
library(wordcloud)

install.packages('SDMTools')
library(SDMTools)

#------Decision trees------
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('ISLR')
library('ISLR')

#ROC chart 
install.packages('pROC')
library(pROC)



# 1.1 Loading the data into R and creating a dataframe
school.raw <- read.csv("test data.csv")
str(school.raw)
summary(school.raw)

school.prepared <- school.raw

# 1.2 Checking to see if we have missing data
any(is.na(school.prepared)) # the answer is false, which means we don't have any missing data

#1.3  Creating graph to check "higher" attribute
ggplot(school.prepared,aes(higher, G3)) + geom_boxplot()
table(school.prepared$G3)
# �� ����� �� ����� ���� ���� ����� ��� ���� ���� ���� 0 ������ ����� �����, ���� �������� ����� ���� ����� ����� 38 �����
#��� ����� ����� �� ������ ��� 0, ��� �� ���� ���� 

#2.1 creating graphs in the EDA 
ggplot(school.prepared,aes(school, G3)) + geom_boxplot()
ggplot(school.prepared,aes(G3, school)) + geom_point()
ggplot(school.prepared,aes(G3 , fill = school)) + geom_bar()
table(school.prepared$school, school.prepared$G3)
# ���� ����� �������� ������� ���� ����� ���� ����� �� ��� ���� ��� ���� ������ ����� ����� ���
# ���� ������� ����� ���� ����� ��� "��" �� �������� ���� ������ ����� ��� ����, ��� ����� �� ��� ���� �� ���� ����� �� ������

#2.2 I choose to leave the abnormal valuse in the dataframe and istead created a histogram with only the values vector
ggplot(school.prepared, aes(absences)) + geom_histogram()
# ���� ���� ����� �� ������ �� ��� ��� ������, ��� ����� �� ������ �� ���� �5 ������
# ����� ������ �� ������� ���� ����� �����, ��� ����� ����� ��� �� ����� �� 5 ������ �����
vec1 <- school.prepared$absences
vec2 <- vec1[vec1[vec1 >= 5]]
hist(vec2)

#2.3 creating a fucntion to mesure success

successfunc <- function(x){
  if (x > 11) {
    return("Yes")}
    else{
      return("No")
    }
}
  
# applying the function to the dataframe and creating a new column with the value success
school.prepared$success <- sapply(school.prepared$G3,successfunc)
str(school.prepared)
ggplot(school.prepared,aes(absences,fill = success)) + geom_bar(position = 'fill')
# ���� ����� ��� ����� �� ��� ��� ��������� ���� �����


ggplot(school.prepared,aes(failures, fill = success)) + geom_bar(position = 'fill')
# �� ����� �� ����� �� ����� ��� ������� ���� ������ ����� 

#3.1 spliting the data into training and test sets 

filter <- sample.split(school.prepared$id, SplitRatio = 0.7)

school.train <- subset(school.prepared, filter == TRUE)
school.test <- subset(school.prepared, filter == FALSE)
dim(school.train)
dim(school.test)

#3.2 creating a linear regression model 
modelLR <- lm(G3~., school.train)
summary(modelLR)
predicted.school.test <- predict(modelLR, school.test)
# mesuring the MSE 
MSE.test <- mean((school.test$id - predicted.school.test)**2)
MSE.test**0.5
# ���� �������� ����� ���� ����� ������ ���� 3 ������ ��� ��������� ������, ����� ��� ������ ����� ����� ��� ����� ���� ����� ������ � underfitting.

summary(predicted.school.test)
summary(modelLR)

ggplot(school.prepared,aes(address,fill = success)) + geom_bar(position = 'fill')
ggplot(school.prepared,aes(sex,fill = success)) + geom_bar(position = 'fill')
ggplot(school.prepared,aes(fill = id,success)) + geom_bar()
mean(school.prepared$G3)
median(school.prepared$G3)
# ��� ����� ������ ��"� ������ ������� ���� ����� ����� ������� ������ ����, ��� ������ ������ ������� ����� ������ ��� ��� 
#��� ��� ������ ���, ����� ��� ������ ���� ����� ����� ������ ����� �������� ����� ������ ������,��� ������ ����� ����� ������ �����
# ���� ����� ������

#4.1 mesuring what in the top 20% grade in the class 
min(school.prepared$G3)
max(school.prepared$G3)
table(school.prepared$G3) 
length(school.prepared$G3)
sum(school.prepared$G3)
?quantile()
quantile(school.prepared$G3,0.8)
# creating a function to determine who is an honor student
isHonor <- function(x){
  if (x >= 14) {
    return(TRUE)
  }
  else{
    return(FALSE)
  }
  
}

school.prepared$honor <- sapply(school.prepared$G3,isHonor)

#4.2 creating a naive base model 
school.train.NB <- subset(school.prepared, filter == TRUE)
school.test.NB <- subset(school.prepared, filter == FALSE)

modelNB <- naiveBayes(honor~., school.train.NB)
summary(modelNB)
# creating a confussion matrix and calculating precision and recall 
predictionNB <- predict(modelNB,school.test.NB, type = "raw")
predictionNB.true <- predictionNB[,"TRUE"]
actual <- school.test.NB$honor
predicted.NB <- predictionNB.true > 0.5
cf <- table(predicted.NB,actual)
presiscionNB <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE']) #0.682
recallNB <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE']) # 1

#4.3 creating a decision tree model
school.train.DT <- subset(school.prepared, filter == TRUE)
school.test.DT <- subset(school.prepared, filter == FALSE)
modelDT <- rpart(honor~.,school.train.DT)
rpart.plot(modelDT,box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predictionDT <- predict(modelDT, school.test.DT)
predictDT <- predictionDT > 0.5
actualDT <- school.test.DT$honor
cfDT <- table(predictDT,actualDT)

#4.4 ROC Chart- comparing between the models 
rocNB <- roc(school.test.NB$honor,predictionNB.true, direction = ">", levels = c('TRUE','FALSE'))
rocDT <- roc(school.test.DT$honor,predictionDT, direction = ">", levels = c('TRUE','FALSE'))
plot(rocNB, col = 'red', main = 'ROC chart')
  par(new=TRUE)
plot(rocDT, col = 'blue', main = 'ROC chart')
auc(rocNB)
auc(rocDT)




